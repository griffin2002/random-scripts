#!/bin/env bash


jq -r '.builds[0].artifact_id|split(":")[1]' {{Packer_output_file}} >> post_build_artifacts.txt

// build a jq query to get the SNAPSHOT ID assocaited with the PAcker build
jq -r '.builds[0].artifacts[0].id|split(":")[1]' {{Packer_output_file}} >> post_build_artifacts.txt
